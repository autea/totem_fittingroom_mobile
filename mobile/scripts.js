var connectViewTimeline;
var navigationViewTimeline;
var couponViewTimeline;
var loaderViewTimeline;
var photoViewTimeline;
var couponSubmitedViewTimeline;
var connectionErrorTimeline;
var connectingTimeline;

var connectButton = document.getElementById('connect-button');
var getCouponButton = document.getElementById('coupon-button');
var submitCouponButton =document.getElementById('submit-coupon-button');

var photoBackground;
var isVoucherAvailable;

var viewsArray = [
		goToLoaderView,
		goToConnectView,
		goToCouponView,
		goToPhotoView,
		goToCouponSubmitedView,
		goToConnectionErrorView,
		goToConnectingView,
		goToNavigationView
	];
var currentView = 0;

function initializeView() {
	checkBrowserVersion();
	createConnectingTimeline();
	createConnectViewTimeline();
	createNavigationViewTimeline();
	createCouponViewTimeline();
	createLoaderViewTimeline();
	createPhotoViewTimeline();
	createCouponSubmitedViewTimeline();
	createConnectionErrorTimeline();
}

window.addEventListener('load', function(){
	initializeView();


	// if (connectButton != null) {
	// 	connectButton.addEventListener('touchstart',function(){
	// 		goToNavigationView();
	// 	})
	// }
	// if (getCouponButton != null) {
	// 	getCouponButton.addEventListener('touchstart',function(){
	// 		goToCouponView();
	// 	})
	// }
	// if (submitCouponButton != null) {
	// 	submitCouponButton.addEventListener('touchstart',function(){
	// 		goToNavigationView();
	// 	})
	// }
})

window.addEventListener('resize', function() {
	resetView();
})

function createConnectViewTimeline() {
	connectViewTimeline = new TimelineLite();
	connectViewTimeline.pause();
	connectViewTimeline.from('#connect-button', 0.5, {marginLeft: "-200vw"});
	connectViewTimeline.from('.connect-view input', 0.5, {marginRight: "-200vw"},0);
	connectViewTimeline.from('.connect-view input, #connect-button', 0.01, {opacity: "0"},0);
	connectViewTimeline.to('.connect-view', 0.01, {zIndex: "101"},0);
}

function resetConnectView() {
	$('.connect-view, .connect-view input, .connect-view button').removeAttr('style');
	createConnectViewTimeline();
	showConnectView();
}

function createNavigationViewTimeline() {
	navigationViewTimeline = new TimelineLite();
	navigationViewTimeline.pause();
	navigationViewTimeline.from('.photo-button', 0.5, {marginLeft: "-200vw"},0);
	navigationViewTimeline.from('.coupon-button', 0.5, {marginRight: "-200vw"}, 0);
	navigationViewTimeline.from('.navigation.left', 0.5, {marginLeft: "-100vw"},0);
	navigationViewTimeline.from('.navigation.right', 0.5, {marginRight:"-100vw"},0);
	navigationViewTimeline.from('.choose-product-image', 0.5, {top:"-100vh"},0);
	navigationViewTimeline.from('.control-view',0.01, {opacity:"0"},0);
	navigationViewTimeline.to('.control-view',0.01, {zIndex:"101"},0);
}
	
function createCouponViewTimeline() {
	couponViewTimeline = new TimelineLite();
	couponViewTimeline.pause();
	couponViewTimeline.from('.coupon-view', 0.5, {top: "-110vh"},0);
	couponViewTimeline.to('.coupon-view', 0.01, {opacity:"1"},0);
	couponViewTimeline.to('.coupon-view', 0.01, {zIndex:"101"},0);
}
function resetCouponView() {
	$('.coupon-view, .coupon-view input, .coupon-view button').removeAttr('style');
	createCouponViewTimeline();
	showCouponView();
}
	
function createLoaderViewTimeline() {
	loaderViewTimeline = new TimelineLite();
	loaderViewTimeline.pause();
	loaderViewTimeline.to('.loader-view', 0.5, {opacity:0}, 0);
	loaderViewTimeline.to('.loader-view', 0.01, {zIndex:"1"}, 0.5);
}

function createPhotoViewTimeline() {
	photoViewTimeline = new TimelineLite();
	photoViewTimeline.pause();
	photoViewTimeline.from('.photo-view',0.5, {opacity:0}, 0);
	photoViewTimeline.to('.photo-view', 0.01, {zIndex:"101"},0);
}

function createCouponSubmitedViewTimeline() {
	couponSubmitedViewTimeline = new TimelineLite();
	couponSubmitedViewTimeline.pause();
	couponSubmitedViewTimeline.from('.coupon-submited-view', 0.5, {opacity:0},0);
	couponSubmitedViewTimeline.to('.coupon-submited-view', 0.01, {zIndex:"101"},0);
}
	
function createConnectionErrorTimeline() {
	connectionErrorTimeline = new TimelineLite();
	connectionErrorTimeline.pause();
	connectionErrorTimeline.from('.connection-error-view', 0.5, {opacity:0},0);
	connectionErrorTimeline.to('.connection-error-view', 0.01, {zIndex:"101"},0);
}

function createConnectingTimeline() {
	connectingTimeline = new TimelineLite();
	connectingTimeline.pause();
	connectingTimeline.from('.connecting-view', 0.5, {opacity:0},0);
	connectingTimeline.to('.connecting-view', 0.01, {zIndex:"101"},0);
}

function showConnectView() {
	connectViewTimeline.play();
}
function hideConnectView() {
	connectViewTimeline.reverse();
}
function showNavigationView(){
	navigationViewTimeline.play();
}
function hideNavigationView() {
	navigationViewTimeline.reverse();
}
function showCouponView() {
	couponViewTimeline.play();
}
function hideCouponView() {
	couponViewTimeline.reverse();
}
function hideLoaderView() {
	loaderViewTimeline.play();
}
function showLoaderView() {
	loaderViewTimeline.reverse();
}
function showPhotoView() {
	photoViewTimeline.play();
}
function hidePhotoView() {
	photoViewTimeline.reverse();
}
function showCouponSubmitedView() {
	couponSubmitedViewTimeline.play();
}
function hideCouponSubmitedView() {
	couponSubmitedViewTimeline.reverse();
}
function showConnectionErrorView() {
	connectionErrorTimeline.play();
}
function hideConnectionErrorView() {
	connectionErrorTimeline.reverse();
}
function showConnectingView() {
	connectingTimeline.play();
}
function hideConnectingView() {
	connectingTimeline.reverse();
}

function goToPhotoView() {
	currentView = 3;
	hideNavigationView();
	hideCouponView();
	hideConnectView();
	hideLoaderView();
	hideConnectionErrorView();
	hideCouponSubmitedView();
	hideConnectingView();
	setTimeout(function(){
		showPhotoView();
	},500);
}

function goToLoaderView() {
	currentView = 0;
	hideNavigationView();
	hideCouponView();
	hideConnectView();
	hidePhotoView();
	hideCouponSubmitedView();
	hideConnectionErrorView();
	hideConnectingView();
	// setTimeout(function(){
	// 	showLoaderView();
	// },500);
	showLoaderView();
}

function goToConnectView() {
	currentView = 1;
	hideNavigationView();
	hideCouponView();
	hideLoaderView();
	hideCouponSubmitedView();
	hideConnectionErrorView();
	hidePhotoView();
	hideConnectingView();
	setTimeout(function(){
		showConnectView();
	},500);
}

function goToNavigationView() {
	currentView = 7;
	hideConnectView();
	hideCouponView();
	hidePhotoView();
	hideCouponSubmitedView();
	hideConnectionErrorView();
	hideLoaderView();
	hideConnectingView();
	setTimeout(function(){
		showNavigationView();
	}, 500);
}

function goToCouponView() {
	currentView = 2;
	hideConnectView();
	hideCouponView();
	hideLoaderView();
	hideCouponSubmitedView();
	hideConnectionErrorView();
	hidePhotoView();
	hideConnectingView();
	setTimeout(function(){
		showCouponView();
	}, 500);
}
function goToCouponSubmitedView() {
	currentView = 4;
	hideConnectView();
	hideCouponView();
	hideLoaderView();
	hidePhotoView();
	hideConnectionErrorView();
	hideNavigationView();
	hideConnectingView();
	setTimeout(function(){
		showCouponSubmitedView();
	}, 500);

}
function goToConnectionErrorView() {
	currentView = 5;
	hideConnectView();
	hideCouponView();
	hideLoaderView();
	hidePhotoView();
	hideNavigationView();
	hideCouponSubmitedView();
	hideConnectingView();
	setTimeout(function(){
		showConnectionErrorView();
	}, 500);
}
function goToConnectingView() {
	currentView = 6;
	hideConnectView();
	hideCouponView();
	hideLoaderView();
	hidePhotoView();
	hideNavigationView();
	hideCouponSubmitedView();
	hideConnectionErrorView();
	setTimeout(function(){
		showConnectingView();
	}, 500);
}

function checkBrowserVersion() {
	var ua = window.navigator.userAgent;
	console.log(ua);
	if (ua.match("Android [1-4].[0-3]")!= null && ua.match("Windows Phone") == null) {
		olderBrowserHandling();

		window.addEventListener("resize", function() {
			olderBrowserHandling();
		});
	}
}

function olderBrowserHandling() {
	$(".connection-error-view .connection-failure-image, .connecting-view .connecting-image").css({"width":0.8*window.innerWidth+"px", "height": 0.8*window.innerWidth+"px"});
	$(".connect-view button, .connect-view input").css("width", 0.4*window.innerWidth);
	$(".connect-view .elements-wrapper").css({"height":0.1*window.innerHeight});
	$(".control-view .elements-wrapper").css({"padding-top":0.3*window.innerHeight});
	$(".control-view .navigation").css({"width":0.2*window.innerWidth, "height":0.3*window.innerHeight});
	$(".control-view .choose-product-image").css({"width":0.5*window.innerWidth, "height":0.3*window.innerHeight, "margin-top":0.3*window.innerHeight});
	$(".control-view .photo-button").css({ "height":0.25*window.innerHeight, "width":0.33*window.innerWidth, "left": 0.33*window.innerWidth});
	$(".control-view .coupon-button").css({ "height":0.19*window.innerHeight, "margin-top": 0.3*window.innerWidth, "margin-bottom": 0.3*window.innerWidth});
	$(".photo-view").css({"height" : window.innerHeight});
	$(".photo-view .elements-wrapper, .photo-view .photo").css({"height": window.innerHeight});
	$(".photo-view .elements-wrapper a, .photo-view .elements-wrapper button").css({"left": 0.1*window.innerHeight});
	$(".view").css({"height": window.innerHeight+"px"});
	$(".loader-view img").css({"margin-top":0.4*window.innerHeight});
	$(".elements-wrapper .kb_logo").css({"height": 0.15*window.innerHeight+"px", "margin-bottom": 0.15*window.innerHeight+"px"});
	$(".rotate-overlay").css({"height": window.innerHeight+"px"});
	$(".coupon-view .elements-wrapper").css({"min-height": window.innerHeight+"px"});
}

function resetView() {
	$("div, button").removeAttr("style");
	connectViewTimeline.clear();
	navigationViewTimeline.clear();
	couponViewTimeline.clear();
	loaderViewTimeline.clear();
	photoViewTimeline.clear();
	couponSubmitedViewTimeline.clear();
	connectionErrorTimeline.clear();
	connectingTimeline.clear();
	$("#photo").css({"background-image": photoBackground});
	if (isVoucherAvailable) {
		$("#coupon-button").fadeIn();
	}
	initializeView();
	var func = viewsArray[currentView];
	func();
}
